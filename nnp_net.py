from pattern.en import parsetree as parsetree_en                                                                                                                     
from pattern.es import parsetree as parsetree_es

from itertools import combinations
import networkx as nx

class CharNet:
    
    def __init__(self, full_text, max_sentence_distance=10, lang='en'):
        if lang == 'en':
            T = parsetree_en(full_text)
        elif lang == 'es':
            T = parsetree_es(full_text)

        names = []

        n = 0
        for s in T:
            for c in s.phrases:
                if c.part_of_speech == 'NP':
                    name = []
                    for w in c.words:
                        if w.part_of_speech == 'NNP':
                            name.append(w.string)
                    if name:
                        names.append((" ".join(name), n))
            n += 1


        self.G = nx.Graph()
        
        for pair in combinations(names, 2):
            distance = abs(pair[0][1] - pair[1][1])
            if distance <= max_sentence_distance:
                s = pair[0][0]
                t = pair[1][0]
                if t != s:
                    w = self.G.get_edge_data(s, t,
                                             default={'w': 0})['w'] \
                        + (max_sentence_distance - distance)
                    self.G.add_edge(s, t, w=w)

        
                    
    
