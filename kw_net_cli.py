#!/usr/bin/env python

import argparse
from kw_net import NNNet
import networkx as nx

parser = argparse.ArgumentParser(
    description="create network of NN words in input text, save it as graphml")

parser.add_argument('--text', type=argparse.FileType('r'), required=True,
                    help='input text')

parser.add_argument('--sentence_distance', default=5, type=int,
                    help='maximum sentence distance to consider for edge creation')

parser.add_argument('--prune', default=0, type=int,
                    help='Prune nodes with k below this number')

parser.add_argument('--lang', default='en',
                    help='language parser, es or en')

parser.add_argument('--graphml', type=argparse.FileType('w'), required=True,
                    help='path to GraphMl output')

args = parser.parse_args()

nnnet = NNNet(args.text.read(), max_sentence_distance=args.sentence_distance, lang=args.lang)

if args.prune > 0:
    for node, k in nx.degree(nnnet.G.copy()): 
        if k < 100: 
            nnnet.G.remove_node(node)

nx.write_graphml_xml(nnnet.G, args.graphml.name, encoding='utf-8')
