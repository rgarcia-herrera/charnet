from flask import Flask, request, Response
from flask import request
from flask import render_template
from nnp_net import CharNet
import networkx as nx
import tempfile
from os import path

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def create_graphml():
    if request.method == 'POST':
        g_chars = CharNet(request.form['fulltext'],
                          max_sentence_distance=int(request.form['max']),
                          lang=request.form['lang'])
        with tempfile.TemporaryDirectory() as tmpdirname:
            xml_path = path.join(tmpdirname, "character_network.graphml")
            nx.write_graphml_lxml(g_chars.G,
                                  xml_path,
                                  encoding='utf-8')
        
            response = Response(open(xml_path, 'r').read(), mimetype='text/xml')
            response.headers.set("Content-Disposition", "attachment", filename="character_network.graphml")
            return response

    return render_template('post_text.html')
