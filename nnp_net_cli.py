#!/usr/bin/env python

import argparse
from nnp_net import CharNet
import networkx as nx

parser = argparse.ArgumentParser(
    description="create network of NNP words in input text, save it as graphml")

parser.add_argument('--text', type=argparse.FileType('r'),
                    help='input text')

parser.add_argument('--max', default=10, type=int,
                    help='maximum sentence distance to consider for edge creation')

parser.add_argument('--lang', default='en',
                    help='language parser, es or en')

parser.add_argument('--graphml', type=argparse.FileType('w'),
                    help='path to GraphMl output')

args = parser.parse_args()

g_chars = CharNet(args.text.read(), max_sentence_distance=args.max, lang=args.lang)

nx.write_graphml_xml(g_chars.G, args.graphml.name, encoding='utf-8')
